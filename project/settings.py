import os
normpath = lambda *args: os.path.normpath(os.path.abspath(os.path.join(*args)))

import settings_local as local


PROJECT_ROOT = normpath(__file__, "../..")

DEBUG = local.DEBUG
TEMPLATE_DEBUG = DEBUG
ALLOWED_HOSTS = local.ALLOWED_HOSTS
SITE_ID = 1
MEDIA_ROOT = normpath(PROJECT_ROOT, "static", "uploads")

DATABASES = {
    'default': {
        'ENGINE': local.DATABASE_ENGINE,
        'NAME': local.DATABASE_NAME,
        'USER': local.DATABASE_USER,
        'PASSWORD': local.DATABASE_PASSWORD,
        'HOST': local.DATABASE_HOST,
        'PORT': local.DATABASE_PORT,
    }
}

MEDIA_URL = '/static/uploads/'
STATIC_ROOT = normpath(PROJECT_ROOT, "static", "static")
STATIC_URL = '/static/static/'
SECRET_KEY = local.SECRET_KEY
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
TEMPLATE_LOADERS = (
    'django.template.loaders.app_directories.Loader',
)
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'cms.context_processors.media',
    'sekizai.context.SekizaiContext',
)
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'cms.middleware.multilingual.MultilingualURLMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
)
ROOT_URLCONF = 'project.urls'
WSGI_APPLICATION = 'wsgi.application'
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'south',

    'cms',
    'menus',
    'mptt',
    'sekizai',

    'point_template',

    'cms.plugins.picture',
    'cms.plugins.file',
    'cms.plugins.link',
    'cms.plugins.snippet',
    'cms.plugins.teaser',
    'cms.plugins.text',
    'cms.plugins.video',

    "form_designer",
    "picklefield",
    'form_designer.contrib.cms_plugins.form_designer_form',
    "easy_thumbnails",
    "inline_ordering",
    "cms_plugins",
    "cms_plugins.gallery",
)


CMS_TEMPLATES = (
    ("cmstemplate/point/header1.html", "header1"),
    ("cmstemplate/point/header2.html", "header2"),
    ("cmstemplate/point/header2a.html", "header2a"),
    ("cmstemplate/point/header3.html", "header3"),
)

LANGUAGES = [
    ('cs', 'Czech'),
]
LANGUAGE_CODE = "cs"
CMS_LANGUAGES = LANGUAGES


LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'
LOGIN_REDIRECT_URL = "/"

gettext = lambda s: s
LANGUAGES = (
    ('cs', 'Czech'),
)

