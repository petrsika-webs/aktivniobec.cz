from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _

from models import GalleryPluginX
import random

class GalleryPluginXX(CMSPluginBase):
    model = GalleryPluginX
    name = _("Gallery X")
    render_template = "cms_plugins/gallery.html"

    def render(self, context, instance, placeholder):
        context.update({
            "random":random.randint(1, 99999999),
            'object':instance.gallery,
            'instance':instance,
            'placeholder':placeholder,
        })  
        return context


plugin_pool.register_plugin(GalleryPluginXX)
